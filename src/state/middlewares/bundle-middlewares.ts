import { Middleware } from 'redux';
import { ActionType } from '../action-types';

// export const bundlerMiddleware = (store) => {
//   return (next) => {
//     return (action) => {

//     }
//   }
// }

let timer: any;

export const bundlerMiddleware: Middleware = ({ getState }) => (next) => (
  action
) => {
  next(action);

  if (action.type !== ActionType.UPDATE_CELL) {
    return;
  }

  const {
    cells: { data: cellData },
  } = getState();
  const cell = cellData[action.payload.id];

  if (cell.type === 'text') {
    return;
  }

  clearTimeout(timer);
  timer = setTimeout(() => {
    console.log('Timer expired.');
  }, 750);
};
